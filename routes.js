const { Router } = require("express");
const controller = require("./controller");

const router = Router();

router.get("/", controller.toGetTodos);
router.post("/", controller.toAddTodo);
router.get("/:id", controller.toGetTodoByID);
router.delete("/:id", controller.toDeleteTodo);
router.put("/:id", controller.toUpdateTodo);

module.exports = router;


