Instructions to Run the code in your system.

# Pre-requisites for the project

- Git
- Node.js (version >= 14)
- Postgres Database
- ORM prisma
- Express

---

### 1. Commands to run before trying the project.

After clone

```bash
 cd express_js_project
 npm install  #It will install the dependencies into your system
```

### 2. Steps to create Postgres database and table

Login into Postgres server using your user name and password.
Type the following commnads in the terminal:

CREATE DATABASE Express_JS_Project

### 3. Configure Environment variables:

Copy .env.example to .env.
Update the .env file with YOUR PostgreSQL database credentials.

### 4. Run the following command to add table to your database:

```bash
npx prisma migrate
```

### 5. Run the server :

```bash
node server.js
```

### 6. Start postman app and try different methods to test:

**Todos**
Get all todos
GET /api/todos
Description:Retrieves all todo items.`

**Get a specific todo**

GET /api/todos/:id
Description:Retrieves a specific todo item based on the provided id parameter.

**Create a todo**

POST /api/todos
Description:Creates a new todo item. The request body should contain the following JSON data:
{
"text": "Task description",
"isCompleted": false
}

**Update a todo**

PUT /api/todos/:id
Description:Updates an existing todo item with the provided id. The request body should contain the updated JSON data:
{
"text": "Updated task description",
"isCompleted": true
}

**Delete a todo**

DELETE /api/todos/:id
Description:Deletes the todo item with the provided id.
Error Handling
