const { PrismaClient } = require("@prisma/client");
const prisma = new PrismaClient()

const yup = require("yup");


//Validation by Yup
const dataSchema = yup.object().shape({
  text: yup.string().required(),
  iscompleted: yup.boolean().required(),
});

const validateID = yup.object().shape({
  inputID: yup.number().required(),
});

//1. To get all the Todos
async function toGetTodos(req, res) {
  try {
    const todos = await prisma.todos.findMany();
    res.send(todos);
  } catch (err) {
    next(err);
  }
}


//2. To get Todo by Id 
async function toGetTodoByID(req, res, next) {
  const id = parseInt(req.params.id);
  try {
    await validateID.validate({ inputID: id });
  } catch (err) {
    return res.status(400).json({ message: "Invalid data" });
  }

  try {
    const todo = await prisma.todos.findUnique({
      where: {
        id: id,
      },
    });
    if (!todo) {
      return res.status(404).json({ message: "Todo is not found." });
    }
    res.status(200).json(todo);
  } catch (err) {
    next(err);
  }
}

// 3. Add a todo in the todos.
async function toAddTodo(req, res, next) {
  try {
    await dataSchema.validate(req.body);
  } catch (err) {
    return res.status(400).json({ message: "Invalid Input" });
  }

  const { text, iscompleted } = req.body;
  try {
    const todo = await prisma.todos.create({
      data: {
        text,
        iscompleted,
      },
    });
    res.status(201).json(todo);
  } catch (err) {
    next(err);
  }
}

// 4. Update an available id todo.
async function toUpdateTodo(req, res, next) {
  const id = parseInt(req.params.id);
  const { text, iscompleted } = req.body;
  try {
    await validateID.validate({ inputID: id });
    await dataSchema.validate(req.body);
  } catch (err) {
    return res.status(400).json({ message: "Invalid data" });
  }

  try {
    const Todo = await prisma.todos.findUnique({
      where: {
        id: id,
      },
    });
    if (!Todo) {
      return res.send(404);
    }
    const todo = await prisma.todos.update({
      where: { id: id },
      data: { text: text, iscompleted: iscompleted },
    });
    res.status(200).json(todo);
  } catch (err) {
    next(err);
  }
}

//5. Remove a todo from todos.
async function toDeleteTodo(req, res, next) {
  const id = parseInt(req.params.id);

  try {
    await validateID.validate({ inputID: id });
  } catch (err) {
    return res.status(400).json({ message: "Invalid data" });
  }

  try {
    const Todo = await prisma.todos.findUnique({
      where: {
        id: id,
      },
    });

    if (!Todo) {
      return res.send(404).json({ message: "Todo is not found." });;
    }

    const todo = await prisma.todos.delete({
      where: {
        id: id,
      },
    });
    res.status(200).json(todo);
  } catch (err) {
    next(err);
  }
}

function errorHandler(err, req, res, next) {
  res.status(500).json({ message: "Internal server error" });
}

module.exports = { toGetTodos, toGetTodoByID, toAddTodo, toDeleteTodo, toUpdateTodo, errorHandler };
