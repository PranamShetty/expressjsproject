const express = require("express");
const dataRoutes = require("./routes");
const bodyParser = require("body-parser");
const controller = require('./controller')

const app = express();

const port = process.env.PORT || 8000;

app.use(bodyParser.json());
app.use(express.json());

app.get("/", (req, res) => {
    res.send("Welcome, On todos server");
});

app.use("/todos", dataRoutes);

app.use(controller.errorHandler);

app.listen(port, () =>
    console.log(`App listening on server port http://localhost:${port}/todos/`)
);

